<?php
/*
 * SesionController.php
 * Copyright ©: 2019 integrasoft.mx. Todos los derechos reservados.
 * Developer(s):
 *      Ing. Janet Aragón González
 * Fecha de creación: 2019-08-15.
 * Fecha de modificación: 2019-08-15.
 * Descripción: Controlador que coordina el módulo de sesión.
 */


namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use app\models\sesion\Sesion;

/**
 * Clase controlador de sesión
 */
class SesionController extends Controller
{
  /**
    * @inheritdoc
    */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        //$this->logu = new BitacoraUsuario();
        //$this->logs = new BitacoraSistema();

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],

        ];
    }

    public function actionStatus() {
    }

  /*
   * Muestra la pantalla principal cuando un usuario inició su sesión.
   */
   public function actionIndex() {
       return $this->render('index');
   }
/*
 * Muestra la pantalla de inicio de sesión
 */
  public function actionIniciar()
  {
    $this->layout = "main_no_registrado";
    $model = new Sesion;
    if (!\Yii::$app->user->isGuest) {
            return $this->render('/sesion/index');
        }

    if($model->load(Yii::$app->request->post())) {
      try{
        if($model->iniciar()){
          $mensaje =  "EL USUARIO " . Yii::$app->user->identity->idUsuario . " INICIÓ SESIÓN";
          //Aqui falta agregar el log
          Yii::$app->session->setFlash('success', "Iniciando sesi&oacute;n...");
          Yii::$app->view->registerJs("setTimeout(function(){ window.location.href ='/site/index' }, 500);");
        } else{
          Yii::$app->session->setFlash('error', "No existe un usuario con ese usuario y contrase&ntilde;a");
        }
      } catch(\Exception $ex){
         Yii::$app->session->setFlash('error', $ex->getMessage());
      }
    } else{
      $model->getErrors();
    }
    return $this->render('iniciar', ['model' => $model]);
  }

public function actionCerrar(){
  $model = new Sesion;

  //$mensaje =  "EL USUARIO " . Yii::$app->user->identity->idUsuario . " CERRÓ SESIÓN";
    //FALTA INCLUIR EL Log

    $model->cerrar();
    Yii::$app->session->setFlash('success', "Cerrando sesi&oacute;n...");
    Yii::$app->view->registerJs(
        "setTimeout(function(){ window.location.href ='/sesion/iniciar' }, 500);");

        return $this->render('cerrar');
}

}?>
