<?php
/*
 * UsuarioController.php
 * Copyright ©: 2019 janetaragon.mx. Todos los derechos reservados.
 * Developer(s):
 *      Ing. Janet Aragón González
 * Fecha de creación: 2019-07-10.
 * Fecha de modificación: 2019-08-27.
 * Descripción: Controlador que coordina el módulo de cambiar contraseña.
 */

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\data\Pagination;

use app\models\log\BitacoraUsuario;
use app\models\log\BitacoraSistema;
//use app\models\usuario\UsuarioSearch;
use app\models\usuario\TokenContrasenia;
use app\models\usuario\CambiarContraseniaForm;
use app\models\usuario\RecuperarContraseniaForm;
use app\models\usuario\Usuario;
use app\models\usuario\Sexo;
use app\models\usuario\TipoUsuario;


class UsuarioController extends \yii\web\Controller
{
  private $logu;
  private $logs;
  public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                   /* [
                        'actions' => [ 'login', 'error', 'auth' ],
                        'allow'   => true,
                    ],*/
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        $this->logu = new BitacoraUsuario();
        $this->logs = new BitacoraSistema();

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            /*'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
            ],*/
        ];
    }

    /*
    * Muestra la pantalla de cambiar contraseña
    */
    public function actionCambiarcontrasenia($token = null){
      if(Yii::$app->user->isGuest && $token == null)
          return $this->redirect('/sesion/iniciar');
      else if($token != null) {
          $this->layout = "main_no_registrado";
      }

      $model = new CambiarContraseniaForm;
      $usuario = null;
      $tokenContrasenia = null;

      if(Yii::$app->user->identity != null)
          $usuario = Yii::$app->user->identity->findIdentity(Yii::$app->user->identity->idUsuario);
      else {
          $tokenContrasenia = TokenContrasenia::findOne(['token' => $token]);

          if($tokenContrasenia != null) {
              $usuario = Usuario::findOne(['idUsuario' => $tokenContrasenia->idUsuario]);
          }
      }

      if($usuario != null) {
          if($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax) {
              Yii::$app->response->format = Response::FORMAT_JSON;
              return ActiveForm::validate($model);
          }
          if($model->load(Yii::$app->request->post())) {
              if($model->validate()) {
                  try {
                      if($usuario->cambiarContrasenia($model->contrasenia, $usuario)) {
                        echo ($tokenContrasenia);
                          if($tokenContrasenia != null) {
                              $tokenContrasenia->eliminar();
                          }
                          $mensaje =  "EL USUARIO " . $usuario->idUsuario . " CAMBIÓ SU CONTRASEÑA";
                          $this->logu->registrar($usuario->idUsuario, $usuario->idTipoUsuario, $mensaje, 1);
                          $this->logs->registrar($usuario->idUsuario, $usuario->idTipoUsuario, "DEBUG",
                              "UsuarioController", $mensaje);

                          Yii::$app->session->setFlash('success', "Se ha cambiado la contrase&ntilde;a");
                      } else {
                          Yii::$app->session->setFlash('error', "No se pudo cambiar la contrase&ntilde;a");
                      }
                      $model->contrasenia = null;
                      $model->confirmaContrasenia = null;
                  } catch(\Exception $ex) {
                      $mensaje =  "EL USUARIO " . Yii::$app->user->identity->idUsuario . " NO CAMBIÓ SU CONTRASEÑA";
                      $this->logu->registrar(Yii::$app->user->identity->idUsuario,
                          Yii::$app->user->identity->idTipoUsuario, $mensaje, 0);
                      $this->logs->registrar(Yii::$app->user->identity->idUsuario,
                          Yii::$app->user->identity->idTipoUsuario, "ERROR", "UsuarioController", $ex->getMessage());
                      Yii::$app->session->setFlash('error', Yii::$app->params['errorInesperado']);
                  }
              } else {
                  $model->getErrors();
              }
          }
      } else {
          Yii::$app->session->setFlash('error', "No existe el enlace al que se quiere ingresar");
      }

      return $this->render('cambiarContrasenia', ['model' => $model, 'token' => $token]);
    }

    /*
     * Muestra la pantalla de recuperar contraseña.
     */

    public function actionRecuperarcontrasenia(){
        if(!\Yii::$app->user->isGuest)
            return $this->redirect('/sesion/');

        $this->layout = "main_no_registrado";
        $model = new RecuperarContraseniaForm();

        if($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if($model->load(Yii::$app->request->post())) {
            if($model->validate()) {
                try {
                    if(Usuario::recuperarContrasenia($model->correoElectronico)) {
                        $correo = Usuario::obtenerXCorreo($model->correoElectronico);
                        $mensaje =  "EL USUARIO " . $correo->idUsuario .
                            " GENERÓ TOKEN DE RECUPERAR CONTRASEÑA";
                        $this->logu->registrar($correo->idUsuario, $correo->idTipoUsuario, $mensaje, 1);
                        $this->logs->registrar($correo->idUsuario, $correo->idTipoUsuario, "DEBUG", "UsuarioController", $mensaje);

                        $model->correoElectronico = null;
                        Yii::$app->session->setFlash('success', "Se ha enviado un correo electrónico " .
                            "para recuperar la contraseña ");

                    } else {
                        Yii::$app->session->setFlash('error', "Error al generar el token de la contraseña");
                    }
                } catch (\Exception $ex) {
                    $mensaje =  "EL USUARIO " . $model->correoElectronico .
                        " NO GENERÓ TOKEN DE RECUPERAR CONTRASEÑA";
                    $this->logu->registrar(0, 0, $mensaje, 0);
                    $this->logs->registrar(0, 0, "ERROR", "UsuarioController", $ex->getMessage());
                    Yii::$app->session->setFlash('error', Yii::$app->params['errorInesperado']);
                }
            } else {
                $model->getErrors();
            }
        }

        return $this->render('recuperarContrasenia', ['model' => $model]);
    }



    public function actionRegistraadministrativo(){
      //if(\Yii::$app->user->isGuest)
          //return $this->redirect('/sesion/');

      $model = new Usuario;
      $tiposUsuarios = ArrayHelper::map(TipoUsuario::find()->where(['idTipoUsuario' => [1, 2, 3]])->all(),"idTipoUsuario","nombreTipoUsuario");
      $sexos = ArrayHelper::map(Sexo::find()->all(),"idSexo", "nombreSexo");
      $session = Yii::$app->session;
      $userAttribute = $session->get('userAttribute');

      if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validate($model);
      }
      if($model->load(Yii::$app->request->post())){
        try {
          $idUsuarioCreate = 1;
          $idUsuarioRecord = 1;
          $model->idTipoUsuario;
          $model->idSexo;
          $model->idEstatusUsuario = 1;
          $model->nombreUsuario;
          $model->apPaterno;
          $model->apMaterno;
          $model->usuario;
          $model->contrasenia = md5($model->contrasenia);
          echo "entra";
          if($model->registrar($idUsuarioCreate, $idUsuarioRecord)){
              echo "registro";
            $mensaje = "EL USUARIO " . $model->idUsuario . "SE REGISTRÓ CORRECTAMENTE";
            $this->logu->registrar($model->idUsuario, $model->idTipoUsuario, $mensaje, 1);
            $this->logs->registrar($model->idUsuario, $model->idTipoUsuario, "DEBUG", "UsuarioController", $mensaje);
            Yii::$app->session->setFlash('success', "Los datos han sido guardados.");
          } else {
            Yii::$app->session->setFlash('error', "Error al registrar usuario.");
          }

        } catch (\Exception $ex) {
          $mensaje = "EL USUARIO " . $model->usuario . "NO SE PUDO REGISTRAR";
          $this->logu->registrar(0, 0, $mensaje, 0);
          $this->logs->registrar(0, 0, "ERROR", "UsuarioController", $ex->getMessage());
          Yii::$app->session->setFlash('error', Yii::$app->params['errorInesperado']);
          throw $ex;
        }

      }
        return $this->render('registraadministrativo',
        [
          'model' => $model,
          'sexos' => $sexos,
          'tiposUsuarios' => $tiposUsuarios
        ]);
    }

}
