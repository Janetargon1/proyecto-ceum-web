<?php
/*
 * IniciarForm.php
 * Copyright ©: 2019 integrasoft.mx. Todos los derechos reservados.
 * Developer(s):
 *      Ing. Janet Aragón González
 * Fecha de creación: 2019-08-15..
 * Fecha de modificación: 2019-08-15..
 * Descripción: Formulario para iniciar sesión.
 */

namespace app\models\sesion;
use Yii;
use yii\base\Model;

use app\models\usuario\Usuario;
/**
 *
 */
class Sesion extends Model
{
  public $usuario;
  public $contrasenia;

  /*
    * Método que define las validaciones de los atributos.
    */
   public function rules() {
       return [
           ['usuario', 'required', 'message' => 'El usuario es obligatorio'],
           ['contrasenia', 'required', 'message' => 'La contraseña es obligatoria']
       ];
   }

   public function attributeLabels() {
       return [
           'usuario' => 'Usuario',
           'contrasenia' => 'Contraseña'
       ];
   }

   public function iniciar() {
        if($this->validate()) {
            $usuario = new Usuario;

            $usuario = $usuario->obtenerXUsuarioContrasenia($this->usuario, md5($this->contrasenia));
            //echo $usuario->idUsuario;
            if($usuario != null) {
                $usuario->id = $usuario->idUsuario;
                return Yii::$app->user->login($usuario, 0);
            } else
                return false;
        }

        return false;
    }

    public function cerrar() {
        Yii::$app->user->logout();
    }
}

?>
