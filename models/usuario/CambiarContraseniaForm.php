<?php
/*
 * CambiarContraseniaForm.php
 * Copyright ©: 2019 janetaragon. Todos los derechos reservados.
 * Developer(s):
 *      Ing. Janet Aragón González
 * Fecha de creación: 2019-08-27.
 * Fecha de modificación: 2019-08-27.
 * Descripción: Formulario para cambiar contraseña.
 */
 namespace app\models\usuario;

 use Yii;
 use yii\base\Model;

 use app\models\usuario\Usuario;

 class CambiarContraseniaForm extends Model
 {
   public $contrasenia;
   public $confirmaContrasenia;

   public function rules() {
       return [
           ['contrasenia', 'required', 'message' => 'La contraseña es obligatoria'],
           ['contrasenia', 'match', 'pattern' => "/^.{8,50}$/",
               'message' => 'La contraseña debe contener entre 8 y 50 caracteres'],
           ['contrasenia', 'verificarContraseniaValidaRules'],
           ['confirmaContrasenia', 'required', 'message' => 'La confirmación de la contraseña es obligatoria'],
           ['confirmaContrasenia', 'compare', 'compareAttribute' => 'contrasenia',
               'message' => 'No coincide la contraseña y su confirmación']
       ];
   }

   public function attributeLabels(){
       return[
           'contrasenia'=>'Contraseña',
           'confirmaContrasenia'=>'Confirmar contraseña'
       ];
   }

   public function verificarContraseniaValidaRules($attribute, $params) {
       if(!Usuario::verificarContraseniaValida($this->contrasenia)) {
           $this->addError($attribute, "La contraseña debe contener por lo menos
                                        dos letras, un número y un caracter especial.");
           return true;
       }
       return false;
   }

 }

?>
