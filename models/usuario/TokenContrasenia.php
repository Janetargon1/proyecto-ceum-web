<?php
/*
 * TokenContrasenia.php
 * Copyright ©: 2019 janetaragon. Todos los derechos reservados.
 * Developer(s):
 *      Ing. Janet Aragón González
 * Fecha de creación: 2019-08-27.
 * Fecha de modificación: 2019-08-27.
 * Descripción: Formulario que genera token para recuperar contraseña.
 */

namespace app\models\usuario;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "TokenContrasenia".
 *
 * @property int $idTokenContrasenia
 * @property int $idUsuario
 * @property string $token
 * @property string $createDate
 * @property string $recordDate
 * @property string $deleteDate
 * @property int $idUsuarioCreate
 * @property int $idUsuarioRecord
 * @property int $idUsuarioDelete
 *
 * @property Usuario $usuario
 */
class TokenContrasenia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'TokenContrasenia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idUsuario', 'token', 'createDate', 'recordDate', 'idUsuarioCreate', 'idUsuarioRecord'], 'required'],
            [['idUsuario', 'idUsuarioCreate', 'idUsuarioRecord', 'idUsuarioDelete'], 'integer'],
            [['createDate', 'recordDate', 'deleteDate'], 'safe'],
            [['token'], 'string', 'max' => 50],
            [['idUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idUsuario' => 'idUsuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idTokenContrasenia' => 'Id Token Contrasenia',
            'idUsuario' => 'Id Usuario',
            'token' => 'Token',
            'createDate' => 'Create Date',
            'recordDate' => 'Record Date',
            'deleteDate' => 'Delete Date',
            'idUsuarioCreate' => 'Id Usuario Create',
            'idUsuarioRecord' => 'Id Usuario Record',
            'idUsuarioDelete' => 'Id Usuario Delete',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['idUsuario' => 'idUsuario']);
    }

    public function registrar($idUsuario, $token, $idUsuarioCreate, $idUsuarioRecord) {
        $this->idUsuario = $idUsuario;
        $this->token = $token;
        $this->createDate = new Expression('GETDATE()');
        $this->recordDate = new Expression('GETDATE()');
        $this->idUsuarioCreate = $idUsuario;
        $this->idUsuarioRecord = $idUsuario;

        if($this->save()) {
            return true;
        } else {
            $error = print_r($this->getErrors(), true);
            throw new \Exception($error);
        }

        return false;
    }

    public function eliminar() {
        if($this->delete()) {
            return true;
        } else {
            $error = print_r($this->getErrors(), true);
            throw new \Exception($error);
        }
    }

    public static function generarToken() {
        return md5(date("BsiHwjzdmY" . str_replace(microtime(true), '', 0)));
    }

    public static function obtenerTokenGeneradoXIdUsuario($idUsuario) {
        return TokenContrasenia::findOne(['idUsuario' => $idUsuario]);
    }
}
