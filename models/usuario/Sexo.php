<?php

namespace app\models\usuario;

use Yii;

/**
 * This is the model class for table "Sexo".
 *
 * @property int $idSexo
 * @property string $nombreSexo
 *
 * @property Usuario[] $usuarios
 */
class Sexo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Sexo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombreSexo'], 'required'],
            [['nombreSexo'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idSexo' => 'Id Sexo',
            'nombreSexo' => 'Nombre Sexo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['idSexo' => 'idSexo']);
    }
}
