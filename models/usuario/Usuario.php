<?php

namespace app\models\usuario;

use Yii;

use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "Usuario".
 *
 * @property int $idUsuario
 * @property int $idTipoUsuario
 * @property int $idSexo
 * @property int $idEstatusUsuario
 * @property string $nombreUsuario
 * @property string $apPaterno
 * @property string $apMaterno
 * @property string $usuario
 * @property string $contrasenia
 * @property string $fechaRegistro
 * @property string $fechaBaja
 * @property string $fechaReactivacion
 * @property string $createDate
 * @property string $recordDate
 * @property string $deleteDate
 * @property int $idUsuarioCreate
 * @property int $idUsuarioRecord
 * @property int $idUsuarioDelete
 * @property string $correo
 *
 * @property TipoUsuario $tipoUsuario
 * @property Sexo $sexo
 * @property EstatusUsuario $estatusUsuario
 */
class Usuario extends ActiveRecord implements IdentityInterface
{
  public $id;
  public $confirmaContrasenia;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Usuario';
    }

    /**
     * {@inheritdoc}
     */
  /*  public function rules()
    {
        return [
            [['idTipoUsurio', 'idSexo', 'idEstatusUsuario', 'nombreUsuario', 'apPaterno', 'usuario', 'contrasenia', 'fechaRegistro', 'createDate', 'recordDate', 'idUsuarioCreate', 'idUsuarioRecord'], 'required'],
            [['idTipoUsurio', 'idSexo', 'idEstatusUsuario', 'idUsuarioCreate', 'idUsuarioRecord', 'idUsuarioDelete'], 'integer'],
            [['fechaRegistro', 'fechaBaja', 'fechaReactivacion', 'createDate', 'recordDate', 'deleteDate'], 'safe'],
            [['nombreUsuario', 'apPaterno', 'apMaterno', 'usuario', 'contrasenia'], 'string', 'max' => 50],
            [['idTipoUsurio'], 'exist', 'skipOnError' => true, 'targetClass' => TipoUsuario::className(), 'targetAttribute' => ['idTipoUsurio' => 'idTipoUsuario']],
            [['idSexo'], 'exist', 'skipOnError' => true, 'targetClass' => Sexo::className(), 'targetAttribute' => ['idSexo' => 'idSexo']],
            [['idEstatusUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => EstatusUsuario::className(), 'targetAttribute' => ['idEstatusUsuario' => 'idEstatusUsuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
  /*  public function attributeLabels()
    {
        return [
            'idUsuario' => 'Id Usuario',
            'idTipoUsurio' => 'Id Tipo Usurio',
            'idSexo' => 'Id Sexo',
            'idEstatusUsuario' => 'Id Estatus Usuario',
            'nombreUsuario' => 'Nombre Usuario',
            'apPaterno' => 'Ap Paterno',
            'apMaterno' => 'Ap Materno',
            'usuario' => 'Usuario',
            'contrasenia' => 'Contrasenia',
            'fechaRegistro' => 'Fecha Registro',
            'fechaBaja' => 'Fecha Baja',
            'fechaReactivacion' => 'Fecha Reactivacion',
            'createDate' => 'Create Date',
            'recordDate' => 'Record Date',
            'deleteDate' => 'Delete Date',
            'idUsuarioCreate' => 'Id Usuario Create',
            'idUsuarioRecord' => 'Id Usuario Record',
            'idUsuarioDelete' => 'Id Usuario Delete',
        ];
    }*/

    public function rules(){
            return [

                ['nombreUsuario','required','message' => 'El campo es obligatorio.'],
                ['apPaterno','required','message' => 'El campo es obligatorio.'],
                ['apMaterno','match', 'pattern' => "/^.{3,50}$/",
                    'message' => 'el apellido materno debe contener entre 3 y 50 caracteres'],
                ['idSexo','required','message' => 'El campo es obligatorio'],
                ['idTipoUsuario','required','message' => 'El campo es obligatorio.'],
                ['usuario','required','message' => 'El campo es obligatorio'],
                ['usuario','username_existe'],
                ['correo','verificarCorreoElectronicoRegistradoRules'],
                ['correo', 'email', 'message' => 'El usuario debe ser un correo electrónico válido'],
                ['contrasenia', 'required', 'message' => 'La contraseña es obligatoria'],
                ['contrasenia', 'match', 'pattern' => "/^.{8,50}$/",
                    'message' => 'La contraseña debe contener entre 8 y 50 caracteres'],
                ['contrasenia', 'verificarContraseniaValidaRules'],
                ['confirmaContrasenia', 'required', 'message' => 'La confirmación de la contraseña es obligatoria'],
                ['confirmaContrasenia', 'compare', 'compareAttribute' => 'contrasenia',
                    'message' => 'No coincide la contraseña y su confirmación']
            ];
        }

        public function attributeLabels(){
            return[
                'idUsuario' => 'ID',
                'idTipoUsuario' => 'Tipo de usuario',
                'nombreUsuario' => 'Nombre',
                'apPaterno' => 'Apellido paterno',
                'apMaterno' => 'Apellido materno',
                'idSexo' => 'Sexo',
                'usuario' => 'Usuario',
                'contrasenia' => 'Contraseña',
                'confirmaContrasenia' => 'Confirma contraseña',
                'createDate' => 'Create date',
                'recordDate' => 'Record date',
                'correo' => 'Correo electrónico',
            ];
        }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoUsuario()
    {
        return $this->hasOne(TipoUsuario::className(), ['idTipoUsuario' => 'idTipoUsuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSexo()
    {
        return $this->hasOne(Sexo::className(), ['idSexo' => 'idSexo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstatusUsuario()
    {
        return $this->hasOne(EstatusUsuario::className(), ['idEstatusUsuario' => 'idEstatusUsuario']);
    }

    public static function findIdentity($id){
      return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {}
    public function getAuthKey() {}
    public function validateAuthKey($authKey) {}

    public function getId(){
      return $this->id;
    }

    public static function verificarContraseniaValida($contrasenia) {
        $coincidencias = null;
        $expresion = "/(?=(.*[a-zA-Z]){2})/";
        preg_match($expresion, $contrasenia, $coincidencias);

        if(sizeof($coincidencias) <= 0)
            return false;

        $coincidencias = null;
        $expresion = "/(?=(.*[0-9]){1})/";
        preg_match($expresion, $contrasenia, $coincidencias);

        if(sizeof($coincidencias) <= 0)
            return false;

        $coincidencias = null;
        $expresion = "/(?=(.*\W){1})/";
        preg_match($expresion, $contrasenia, $coincidencias);

        if(sizeof($coincidencias) <= 0)
            return false;

        $coincidencias = null;
        $expresion = "/[\s]/";
        preg_match($expresion, $contrasenia, $coincidencias);

        if(sizeof($coincidencias) > 0)
            return false;

        return true;
    }

    public function verificarContraseniaValidaRules($attribute, $params) {
        if(!Usuario::verificarContraseniaValida($this->contrasenia)) {
            $this->addError($attribute, "La contraseña debe contener por lo menos
                                         dos letras, un número y un caracter especial.");
            return true;
        }
        return false;
    }

    public function obtenerXUsuarioContrasenia($usuario, $contrasenia) {
        return Usuario::findOne(['usuario' => $usuario, 'contrasenia' => $contrasenia]);
    }

    public static function obtenerXUsuario($usuario) {
        return Usuario::findOne(['usuario' => $usuario]);
    }

    public static function obtenerXCorreo($correo) {
        return Usuario::findOne(['correo' => $correo]);
    }

    public static function verificarCorreoElectronicoNoRegistrado($correo) {
        if(Usuario::obtenerXCorreo($correo) != null)
            return true;
        else
            return false;

        return false;
    }

    public function verificarCorreoElectronicoRegistradoRules($attribute, $params) {
        if(Usuario::verificarCorreoElectronicoNoRegistrado($this->correo)) {
            $this->addError($attribute, "Ya existe una persona registrada con ese correo electrónico.");
            return true;
        }
        return false;
    }

    public function username_existe($attribute, $params)
    {
      if(Usuario::obtenerXUsuario($this->usuario))
      {
        $this->addError($attribute, "El usuario ya existe.");
        return true;
      } else {
        return false;
      }

    }

    public function registrar($idUsuarioCreate, $idUsuarioRecord){
      $this->fechaRegistro = new Expression('GETDATE()');
      $this->createDate = new Expression('GETDATE()');
      $this->recordDate = new Expression('GETDATE()');
      $this->idUsuarioCreate = $idUsuarioCreate;
      $this->idUsuarioRecord = $idUsuarioRecord;

      if($this->save(false)){
        return true;
      } else {
        $error = print_r($this->getErrors(), true);
        throw new \Exception($error);
      }
      return false;
    }

    public static function recuperarContrasenia($correoElectronico) {
        $usuario = Usuario::obtenerXCorreo($correoElectronico);
        $token = null;

        if($usuario != null) {
            $tokenGenerado = TokenContrasenia::obtenerTokenGeneradoXIdUsuario($usuario->idUsuario);

            if($tokenGenerado == null) {
                $tokenGenerado = new TokenContrasenia();
                $token = TokenContrasenia::generarToken();

                if(!$tokenGenerado->registrar($usuario->idUsuario, $token, $usuario->idUsuario, $usuario->idUsuario)) {
                    $error = print_r($this->getErrors(), true);
                    throw new \Exception($error);
                }
            } else {
                $token = $tokenGenerado->token;
            }

            $asunto = "Recuperar contraseña";
            $mensaje = "Se ha creado un enlace para recuperar la contraseña. Por favor presiona " .
                "<a href='http://" . $_SERVER['HTTP_HOST'] .
                "/usuario/cambiarcontrasenia?token=" . $token . "'>AQUÍ</a>.";

            Yii::$app->mailer->compose()
            ->setTo($usuario->correo)
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject($asunto)
            ->setHtmlBody($mensaje)
            ->send();

            return true;

        }

        return false;
    }
}
