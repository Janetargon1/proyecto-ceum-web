<?php

namespace app\models\usuario;

use Yii;

/**
 * This is the model class for table "EstatusUsuario".
 *
 * @property int $idEstatusUsuario
 * @property string $nombreEstatusUsuario
 *
 * @property Usuario[] $usuarios
 */
class EstatusUsuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'EstatusUsuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombreEstatusUsuario'], 'required'],
            [['nombreEstatusUsuario'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idEstatusUsuario' => 'Id Estatus Usuario',
            'nombreEstatusUsuario' => 'Nombre Estatus Usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['idEstatusUsuario' => 'idEstatusUsuario']);
    }
}
