<?php

namespace app\models\usuario;

use Yii;

/**
 * This is the model class for table "TipoUsuario".
 *
 * @property int $idTipoUsuario
 * @property string $nombreTipoUsuario
 *
 * @property Usuario[] $usuarios
 */
class TipoUsuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'TipoUsuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombreTipoUsuario'], 'required'],
            [['nombreTipoUsuario'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idTipoUsuario' => 'Id Tipo Usuario',
            'nombreTipoUsuario' => 'Nombre Tipo Usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['idTipoUsurio' => 'idTipoUsuario']);
    }
}
