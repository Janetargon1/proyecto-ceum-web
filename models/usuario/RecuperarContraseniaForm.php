<?php
/*
 * RecuperarContraseniaForm.php
 * Copyright ©: 2019 janetaragon.mx. Todos los derechos reservados.
 * Developer(s):
 *      Ing. Janet Aragón González
 * Fecha de creación: 2019-08-14.
 * Fecha de modificación: 2019-08-14.
 * Descripción: Formulario para cambiar la contraseña.
 */
namespace app\models\usuario;

use Yii;
use yii\base\Model;

use app\models\usuario\Usuario;

class RecuperarContraseniaForm extends Model{
    public $correoElectronico;

    /*
     * Método que define las validaciones de los atributos
     */
    public function rules(){
        return [
            ['correoElectronico','required','message'=>'El correo electrónico es obligatorio'],
            ['correoElectronico', 'verificarCorreoElectronicoNoRegistradoRules']
        ];
    }

    public function attributeLabels(){
        return[
            'correoElectronico'=>'Correo electrónico'

        ];
    }

    public function verificarCorreoElectronicoNoRegistradoRules($attribute, $params) {
        if(!Usuario::verificarCorreoElectronicoNoRegistrado($this->correoElectronico)) {
            $this->addError($attribute, "No existe");
            return true;
        }

        return false;
    }
}
?>
