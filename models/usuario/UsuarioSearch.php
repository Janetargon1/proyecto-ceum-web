<?php
namespace app\models\usuario;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

use app\models\usuario\Usuario;

/**
 *
 */
class UsuarioSearch extends Usuario
{

  public function rules() {
             return [
                 [['usuario'], 'safe'],
                 [['nombreUsuario'], 'safe'],
                 [['apPaterno'], 'safe'],
                 [['apMaterno'], 'safe'],
                 [['idSexo'], 'integer'],
                 [['createDate'], 'safe'],
                 [['recordDate'], 'safe']
             ];
         }

         public function scenarios() {
             return Model::scenarios();
         }
}
public function search($params) {
  $query = Usuario::find();

  $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
          'pageSize' => 3,
      ],
  ]);

  $this->load($params);

  if (!$this->validate()) {
      // uncomment the following line if you do not want to return any records when validation fails
      // $query->where('0=1');
      return $dataProvider;
  }

  $query->andFilterWhere(['like', 'usuario', $this->usuario]);
  $query->andFilterWhere(['like', 'nombreUsuario', $this->nombre]);
  $query->andFilterWhere(['like', 'apPaterno', $this->apPaterno]);
  $query->andFilterWhere(['like', 'apMaterno', $this->apMaterno]);
  $query->andFilterWhere(['=', 'idSexo', $this->idSexo]);
  $query->andFilterWhere(['=', 'idTipoUsuario', $this->idTipoUsuario]);
  $query->andFilterWhere(['>=', 'createDate', $this->createDate]);
  $query->andFilterWhere(['>=', 'recordDate', $this->recordDate]);
  $query->orderBy('usuario');

  return $dataProvider;
  }
}
?>
