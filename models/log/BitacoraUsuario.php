<?php
/*
 * BitacoraUsuario.php
 * Copyright ©: 2019 janetaragon.me. Todos los derechos reservados.
 * Developer(s): Ing. Janet Aragón González
 * Fecha de creación: 2019-08-14.
 * Fecha de modificación: 2019-08-14.
 * Descripción: Servicio.
 */

 namespace app\models\log;

 use Yii;
 use yii\db\Expression;

 /**
  * This is the model class for table "DatBitacoraUsuario".
  *
  * @property int $idBitacoraUsuario
  * @property string $fechaOperacion
  * @property int $idUsuario
  * @property int $idTipoUsuario
  * @property string $remoteIP
  * @property string $mensaje
  * @property int $operacionExitosa
  */

  class BitacoraUsuario extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'DatBitacoraUsuario';
    }

    public static function getDb() {
        return Yii::$app->get('db_logs');
    }

    public function rules() {
        return [
            [['fechaOperacion', 'idUsuario', 'remoteIP', 'mensaje', 'operacionExitosa'], 'required'],
            [['fechaOperacion'], 'safe'],
            [['idUsuario', 'idTipoUsuario', 'operacionExitosa'], 'integer'],
            [['remoteIP', 'mensaje'], 'string'],
        ];
    }

    public function attributeLabels() {
        return [
            'idBitacoraUsuario' => 'ID',
            'fechaOperacion' => 'Fecha Operacion',
            'idUsuario' => 'Usuario',
            'idTipoUsuario' => 'Tipo de usuario',
            'remoteIP' => 'Remote Ip',
            'mensaje' => 'Mensaje',
            'operacionExitosa' => 'Operacion Exitosa',
        ];
    }

    /*
     * Método que registra en la bitácora de usuario.
     */
    public function registrar($idUsuario, $idTipoUsuario, $mensaje, $operacionExitosa) {
        $this->idUsuario = $idUsuario;
        $this->idTipoUsuario = $idTipoUsuario;
        $this->mensaje = $mensaje;
        $this->operacionExitosa = $operacionExitosa;
        $this->fechaOperacion = new Expression('GETDATE()');
        $this->remoteIP = Yii::$app->params['ip'];

        if(!$this->save(false)) {
            $error = print_r($this->getErrors(), true);
            throw new \Exception($error);
        }
    }
  }
?>
