<?php
/*
 * BitacoraSistema.php
 *
 * Copyright ©: 2019 janetaragon. Todos los derechos reservados.
 * Developer(s): Ing. Janet Aragón González.
 * Fecha de creación: 2019-08-14.
 * Fecha de modificación: 2019-08-14.
 * Descripción: Servicio.
 */

namespace app\models\log;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "AplBitacoraSistema".
 *
 * @property int $idBitacoraSistema
 * @property int $idUsuario
 * @property int $idTipoUsuario
 * @property string $dated
 * @property string $remoteIP
 * @property string $levelMessage
 * @property string $logger
 * @property string $message
 */
class BitacoraSistema extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'AplBitacoraSistema';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_logs');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idUsuario', 'idTipoUsuario'], 'integer'],
            [['dated', 'levelMessage', 'logger', 'message'], 'required'],
            [['dated'], 'safe'],
            [['message'], 'string'],
            [['remoteIP'], 'string', 'max' => 50],
            [['levelMessage'], 'string', 'max' => 10],
            [['logger'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idBitacoraSistema' => 'Id Bitacora Sistema',
            'idUsuario' => 'Id Usuario',
            'idTipoUsuario' => 'Id Tipo Usuario',
            'dated' => 'Dated',
            'remoteIP' => 'Remote Ip',
            'levelMessage' => 'Level Message',
            'logger' => 'Logger',
            'message' => 'Message',
        ];
    }

    /*
     * Método que registra en la bitácora de sistema.
     */

     public function registrar($idUsuario, $idTipoUsuario, $levelMessage, $logger, $message)
     {
       $this->idUsuario = $idUsuario;
       $this->idTipoUsuario = $idTipoUsuario;
       $this->levelMessage = $levelMessage;
       $this->logger = $logger;
       $this->message = $message;
       $this->dated = new Expression('GETDATE()');
       $this->remoteIP = Yii::$app->params['ip'];

       if(!$this->save(false)) {
           $error = print_r($this->getErrors(), true);
           throw new \Exception($error);
       }
     }
}
