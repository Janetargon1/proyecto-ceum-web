<?php
/*
 * iniciar.php
 * Copyright ©: 2019 SoftDV. Todos los derechos reservados.
 * Developer(s):
 *      Ing. Janet Aragón González
 * Fecha de creación: 2019-08-15.
 * Fecha de modificación: 2019-08-15.
 * Descripción: Interfaz principal para iniciar sesión.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'method' => 'post',
    'id' => 'iniciar-sesion-formulario',
    'enableClientValidation' => true
    //'enableAjaxValidation' => true
]);
?>

<h1>Iniciar sesi&oacute;n</h1>
<hr>

<div class="group-form">
    <?= $form->field($model, 'usuario')->input('text', ['maxlength' => 50]) ?>
</div>

<div class="group-form">
    <?= $form->field($model, 'contrasenia')->input('password', ['maxlength' => 50]) ?>
</div>

<?=
Html::submitButton('Iniciar sesi&oacute;n',
    ['class' => 'btn btn-success', 'title' => 'Iniciar sesión...'])
?>

<?=
Html::a("¿Olvidaste tu contrase&ntilde;a?", "/index.php?r=usuario/recuperarcontrasenia",
    ['title' => 'Recuperar contraseña...'])
?>
 <?php $form->end(); ?>
