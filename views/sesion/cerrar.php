<?php
/*
 * cerrar.php
 * Copyright ©: 2019 integrasoft.mx. Todos los derechos reservados.
 * Developer(s):
 *      Ing. Janet Aragón González
 * Fecha de creación: 2019-07-30.
 * Fecha de modificación: 2019-07-30.
 * Descripción: Interfaz que indica que se está cerrando una sesión.
 */
?>
