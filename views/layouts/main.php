<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php //$this->registerCsrfMetaTags() ?>
  <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link href="/css/navbar-fixed-left.min.css" rel="stylesheet"></head>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-nav navbar-fixed-left',
        ],
    ]);
    /*echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();*/

    if(Yii::$app->user->isGuest){
      $itemsMenu[] = ['label' => 'Iniciar sesión', 'url' => ['/sesion/iniciar']];
      //$itemsMenu[] = ['label' => 'Créditos', 'url' => ['/sesion/creditos']];
      //$itemsMenu[] = ['label' => 'Cerrar sesión', 'url' => ['/sesion/cerrar']];
    } elseif(Yii::$app->user->identity->idTipoUsuario == 1){//SUPER ADMINISTRADOR
      //$itemsMenu[] = ['label' => 'Inicio', 'url' => ['/usuario/']];
      $itemsMenu[] = ['label' => 'Registrar perfil administrativo', 'url' => ['/usuario/registraadministrativo']];



      $itemsMenu[] = ['label' => 'Cambiar contraseña', 'url' => ['/usuario/cambiarcontrasenia']];
      $itemsMenu[] = ['label' => 'Créditos', 'url' => ['/site/creditos']];
      $itemsMenu[] = ['label' => 'Cerrar sesión (' . Yii::$app->user->identity->nombreUsuario . ')',
          'url' => ['/sesion/cerrar']];
    } elseif (Yii::$app->user->identity->idTipoUsuario == 2) { //ADMINISTRADOR
      //$itemsMenu[] = ['label' => 'Inicio', 'url' => ['/sesion/']];


      $itemsMenu[] = ['label' => 'Cambiar contraseña', 'url' => ['/usuario/cambiarcontrasenia']];
      $itemsMenu[] = ['label' => 'Créditos', 'url' => ['/site/creditos']];
      $itemsMenu[] = ['label' => 'Cerrar sesión (' . Yii::$app->user->identity->nombreUsuario . ')',
          'url' => ['/sesion/cerrar']];
    } elseif (Yii::$app->user->identity->idTipoUsuario == 3) { //SERVICIOS ESCOLARES
      //$itemsMenu[] = ['label' => 'Inicio', 'url' => ['/sesion/']];
      $itemsMenu[] = ['label' => 'Registrar alumno', 'url' => ['/usuario/registraperfilalumno']];

      $itemsMenu[] = ['label' => 'Cambiar contraseña', 'url' => ['/usuario/cambiarcontrasenia']];
      $itemsMenu[] = ['label' => 'Créditos', 'url' => ['/site/creditos']];
      $itemsMenu[] = ['label' => 'Cerrar sesión (' . Yii::$app->user->identity->nombreUsuario . ')',
          'url' => ['/sesion/cerrar']];
    } elseif (Yii::$app->user->identity->idTipoUsuario == 4) { //ALUMNOS
      //$itemsMenu[] = ['label' => 'Inicio', 'url' => ['/sesion/']];

      $itemsMenu[] = ['label' => 'Cambiar contraseña', 'url' => ['/usuario/cambiarcontrasenia']];
      $itemsMenu[] = ['label' => 'Créditos', 'url' => ['/site/creditos']];
      $itemsMenu[] = ['label' => 'Cerrar sesión (' . Yii::$app->user->identity->nombreUsuario . ')',
          'url' => ['/sesion/cerrar']];
    }

    $menu = Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $itemsMenu,
    ]);

    echo $menu;
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; CEUM <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
