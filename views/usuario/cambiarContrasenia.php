<?php
/*
 * cambiarContrasenia.php
 * Copyright ©: 2019 janetaragon. Todos los derechos reservados.
 * Developer(s):
 *      Ing. Janet Aragón González
 * Fecha de creación: 2019-08-27.
 * Fecha de modificación: 2019-08-27.
 * Descripción: Interfaz para cambiar contraseña.
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

//Inicia el formulario
$form = ActiveForm::begin([
    'method' => 'post',
    'id' => 'iniciar-sesion-formulario',
    //'enableClientValidation' => true
    'enableAjaxValidation' => true
]);
?>
<h1>Cambiar contraseña</h1>
<hr>
<?= Html::a('Regresar', ['/usuario/index'], ['class'=>'btn btn-primary']) ?>
<hr>

<div class="group-form">
    <?= $form->field($model, 'contrasenia')->input('password', ['maxlength' => 50]) ?>
</div>
<div class="group-form">
    <?= $form->field($model, 'confirmaContrasenia')->input('password', ['maxlength' => 50]) ?>
</div>
<?=
Html::submitButton('Cambiar contraseña',
    ['class' => 'btn btn-success', 'title' => 'Cambiar contraseña...'])
?>

<?= Html::a('Regresar', ['/usuario/index'], ['class'=>'btn btn-primary']) ?>

<?php $form->end(); ?>
