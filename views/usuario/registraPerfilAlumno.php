<?php
/*
 * registrarPerfilBasico.php
 * Copyright ©: 2019 Janet Aragón González. Todos los derechos reservados.
 * Developer(s):
 *      Ing. Janet Aragón González
 * Fecha de creación: 2019-08-14.
 * Fecha de modificación: 2019-08-14.
 * Descripción: Interfaz para registrar perfil administrativo del usuario no registrado.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

//Inicia el formulario
$form = ActiveForm::begin([
    'method' => 'post',
    'id' => 'iniciar-sesion-formulario',
    //'enableClientValidation' => true
    'enableAjaxValidation' => true
]);
?>

<h1>Registrar perfil</h1>
<hr>
<?= Html::a('Regresar', ['/sesion/iniciar'], ['class'=>'btn btn-primary']) ?>
<hr>

<div class="group-form">
    <?= $form->field($model, 'nombreUsuario')->input('text', ['maxlength' => 50])->label('* Nombre') ?>
</div>

<div class="group-form">
    <?= $form->field($model, 'apPaterno')->input('text', ['maxlength' => 50])->label('* Apellido paterno') ?>
</div>

<div class="group-form">
    <?= $form->field($model, 'apMaterno')->input('text', ['maxlength' => 50]) ?>
</div>

<div class="group-form">
   <?= $form->field($model, 'idSexo')->dropDownList($sexos)->label('* Sexo') ?>
</div>

<div class="group-form">
    <?= $form->field($model, 'idTipoUsuario')->dropDownList($tiposUsuarios)->label('* Tipo de usuario') ?>
</div>

<div class="group-form">
    <?= $form->field($model, 'usuario')->input('text', ['maxlength' => 50])->label('* Usuario') ?>
</div>

<div class="group-form">
    <?= $form->field($model, 'contrasenia')->input('password', ['maxlength' => 50])->label('* Contraseña') ?>
</div>

<div class="group-form">
    <?= $form->field($model, 'confirmaContrasenia')->input('password', ['maxlength' => 50])->label('* Confirmar contraseña') ?>
</div>

<?=
Html::submitButton('Registrar',
    ['class' => 'btn btn-success', 'title' => 'Registrar...'])
?>

<?= Html::a('Regresar', ['/sesion/iniciar'], ['class'=>'btn btn-primary']) ?>



<?php $form->end(); ?>
