<?php
/*
 * recuperarContrasenia.php
 * Copyright ©: 2019 janetaragon. Todos los derechos reservados.
 * Developer(s):
 *      Janet Aragón González
 * Fecha de creación: 2019-08-14.
 * Fecha de modificación: 2019-08-14.
 * Descripción: Interfaz para cambiar contraseña.
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'method' => 'post',
    'id' => 'iniciar-sesion-formulario',
    //'enableClientValidation' => true
    'enableAjaxValidation' => true
]);
?>

<h1>Recuperar contraseña</h1>
<hr>
<div class="group-form">
<?= $form->field($model, 'correoElectronico')->input('text', ['maxlength'=>50]) ?>
</div>
<?=
Html::submitButton('Recuperar contrasenia',
    ['class' => 'btn btn-success', 'title' => 'Recuperar contraseña...'])
?>

<?php $form->end(); ?>
