<?php

return [
    'adminEmail' => 'admin@example.com',
    'ip' => $_SERVER['REMOTE_ADDR'],
    'version' => '1.0.0',
    'errorInesperado' => 'Ocurri&oacute; un error inesperado. Contacte al administrador del sistema',
    'sinPermisos' => 'No tiene permisos para esta sección',

    //'rutaArchivosReclutamiento' => 'archivosReclutamiento/',
    //'archivoReclutamiento' => 'archivoReclutamiento',
    //'senderEmail' => 'noreply@example.com',
    //'senderName' => 'Example.com mailer',
];
